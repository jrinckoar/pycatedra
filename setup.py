#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='pycatedra',
      version='1.0',
      description='Generador de planilla de resultados.',
      long_description=readme(),
      url='https://jrinckoar@bitbucket.org/jrinckoar/pycatedra.git',
      author='Juan F. Restrepo',
      author_email='jrestrepo@ingenieria.uner.edu.ar',
      license='MIT',
      packages=['pycatedra'],
      entry_points={
          'console_scripts': ['pycatedra = pycatedra.__main__:main']
      },
      install_requires=[
          'xlrd',
          'xlsxwriter',
          'argparse',
          'pylatex',
          'unidecode',
      ],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      zip_safe=False)
