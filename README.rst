============
PYCATEDRA
============
Autor: Juan F. Restrepo

Generador de planillas de resultados para las asignaturas de 
Cálculo en una Variable y Álgebra Lineal y Geometría analítica.

pycatedra.py
    Interfaz gráfica para generar la planilla de cursado de cátedra.

    funciones:
        1. Generar Planilla
           Script para generar la planilla de resultados a partir de los
           archivos descargados del SIU GUARANI.
        2. Script para publicar resultados contenidos en la planilla de
           resltados.

2018-07-29  
'Juan Felipe Restrepo <jrestrepo@ingenieria.edu.ar>'
