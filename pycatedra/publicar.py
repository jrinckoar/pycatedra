#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
publicar.py
    Script para publicar resultados contenidos en la planilla de resltados.

'Juan Felipe Restrepo <jrestrepo@ingenieria.une.edu.ar>'
2018-07-16
'''

import time
import xlrd
import os
from pylatex import Document, LongTabu, Command, NewPage
from pylatex.utils import bold, NoEscape


def error_comision(Lines, comisiones):
    Lines_t = []
    com_t = []
    j = 0
    for i in comisiones:
        if i not in [1, 2, 3, 4]:
            com_t.append(i)
            Lines_t.append(Lines[j + 2])
        j = j + 1
    message = 'error en numero de comision:\n'
    for i, j in zip(Lines_t, com_t):
        message = message + '{0} - comision {1}\n'.format(i[1], j)
    print(message)

    return message


def genenerate_longtabu(Lines, titulo, oname, tab, geometry_options):
    print('Publicando: ' + titulo)
    doc = Document(page_numbers=True, geometry_options=geometry_options)
    doc.preamble.append(Command('title', titulo))
    doc.preamble.append(Command('date', NoEscape(r'\vspace{-10ex}')))
    doc.append(NoEscape(r'\maketitle'))

    # Generate data table
    with doc.create(LongTabu(tab)) as data_table:
        header_row1 = Lines[0]
        del Lines[0]
        data_table.add_hline()
        data_table.add_row(header_row1, mapper=[bold])
        data_table.add_hline()
        # data_table.add_empty_row()
        data_table.end_table_header()
        for i in Lines:
            data_table.add_row(i)
            data_table.add_hline()

    doc.generate_pdf(oname, clean_tex=True)


def generara_tabla_asistencia(Lines, titulo, oname, geometry_options, tab):

    print('Publicando: ' + titulo)
    doc = Document(page_numbers=True, geometry_options=geometry_options)
    doc.preamble.append(Command('title', titulo))
    doc.preamble.append(Command('date', NoEscape(r'\vspace{-10ex}')))
    doc.append(NoEscape(r'\maketitle'))

    # Generar tabla
    n = int(len(Lines[0]) / 2) + 1
    for i in range(2):
        Lines_t = []
        for l in Lines:
            temp = l[0:2]
            if i == 0:
                temp = temp + l[2:n]
            else:
                temp = temp + l[n:]
            Lines_t.append(temp)
        with doc.create(LongTabu(tab)) as data_table:
            header_row1 = Lines_t[0]
            data_table.add_hline()
            data_table.add_row(header_row1, mapper=[bold])
            data_table.add_hline()
            header_row2 = Lines_t[1]
            data_table.add_hline()
            data_table.add_row(header_row2, mapper=[bold])
            data_table.add_hline()
            data_table.end_table_header()
            for i in Lines_t[2:]:
                data_table.add_row(i)
                data_table.add_hline()
        doc.append(NewPage())

    doc.generate_pdf(oname, clean_tex=True)


def read_data(fname, sheet_name):

    wb = xlrd.open_workbook(fname)
    wb_sheet = wb.sheet_by_name(sheet_name)
    materia = wb_sheet.cell(1, 0).value
    materia = materia.strip()
    nrows = wb_sheet.nrows
    comision = []
    Lines = []
    for i in range(2, nrows):
        line = []
        row = wb_sheet.row_slice(rowx=i, start_colx=0)
        line = [j.value for j in row]
        comision.append(line[3])
        Lines.append(line)

    return Lines, comision, materia


def parciales_recuperatorios(fname, sheet_name, titulo, oname, odir):

    date = time.strftime("%d/%m/%Y")

    # Leer datos de planilla
    Lines, comision, materia = read_data(fname, sheet_name)

    for l in Lines:
        del l[0]        # remover carrera
        del l[1]        # remover nombre
        del l[1]        # remover comision
        del l[1]        # remover asistencia
    del comision[0]     # remover 'comision'

    # Titulo de archivo de salida y nombre
    titulo = titulo.format(materia, date)
    if materia == 'Cálculo en una Variable':
        oname = oname.format('Calc', date.replace('/', ''))
    else:
        oname = oname.format('Alg', date.replace('/', ''))
    oname = os.path.join(odir, oname)

    # Organización de la tabla
    tab = '| X[1, c] |'
    tab = tab + ' X[1,c] |' * (len(Lines[0]) - 1)
    # Opciones de tabla latex
    geometry_options = {'a4paper': True, 'landscape': True,
                        'top': '40pt',
                        'left': '30pt',
                        'right': '30pt',
                        'bottom': '40pt'}

    genenerate_longtabu(Lines, titulo, oname, tab, geometry_options)
    return titulo, oname


def condicion_prerec(fname, sheet_name, titulo, oname, odir):

    date = time.strftime("%d/%m/%Y")

    # Leer datos de planilla
    Lines, comision, materia = read_data(fname, sheet_name)
    for l in Lines:
        del l[0]        # remover carrera
        del l[1]        # remover nombre
        del l[1]        # remover comision
    del comision[0]     # remover 'comision'

    # Titulo de archivo de salida y nombre
    titulo = titulo.format(materia, date)
    if materia == 'Cálculo en una Variable':
        oname = oname.format('Calc', date.replace('/', ''))
    else:
        oname = oname.format('Alg', date.replace('/', ''))
    oname = os.path.join(odir, oname)

    # Organización de la tabla
    tab = '| X[1,c] |'
    tab = tab + ' X[1,c] |' * (len(Lines[0]) - 2)
    tab = tab + '|| X[4,l] |'
    # Opciones de tabla latex
    geometry_options = {'a4paper': True, 'landscape': True,
                        'top': '40pt',
                        'left': '30pt',
                        'right': '30pt',
                        'bottom': '40pt'}

    genenerate_longtabu(Lines, titulo, oname, tab, geometry_options)
    return titulo, oname


def condicion_final(fname, sheet_name, titulo, oname, odir):

    date = time.strftime("%d/%m/%Y")

    # Leer datos de planilla
    Lines, comision, materia = read_data(fname, sheet_name)
    for l in Lines:
        del l[0]        # remover carrera
        del l[1]        # remover nombre
        del l[1]        # remover comision
    del comision[0]     # remover 'comision'

    # Titulo de archivo de salida y nombre
    titulo = titulo.format(materia, date)
    if materia == 'Cálculo en una Variable':
        oname = oname.format('Calc', date.replace('/', ''))
    else:
        oname = oname.format('Alg', date.replace('/', ''))
    oname = os.path.join(odir, oname)

    # Organización de la tabla
    tab = '| X[1.2,c] |'
    tab = tab + ' X[1,c] |' * (len(Lines[0]) - 2)
    tab = tab + '|| X[4,l] |'
    # Opciones de tabla latex
    geometry_options = {'a4paper': True, 'landscape': True,
                        'top': '40pt',
                        'left': '30pt',
                        'right': '30pt',
                        'bottom': '40pt'}

    genenerate_longtabu(Lines, titulo, oname, tab, geometry_options)
    return titulo, oname


def asistencia_parciales(fname, sheet_name, titulo, oname, odir):

    date = time.strftime("%d/%m/%Y")

    # Leer datos de planilla
    Lines, comision, materia = read_data(fname, sheet_name)
    for l in Lines:
        del l[-1]        # remover carrera
        del l[-1]        # remover nombre

    # Remover datos numéricos y escribir comision como entero
    for l in Lines[1:]:
        l[3] = int(l[3])
        l[4:] = [' '] * (len(l) - 4)

    # Titulo de archivo de salida y nombre
    titulo = titulo.format(materia, date)
    if materia == 'Cálculo en una Variable':
        oname = oname.format('Calc', date.replace('/', ''))
    else:
        oname = oname.format('Alg', date.replace('/', ''))
    oname = os.path.join(odir, oname)

    # Organización de la tabla
    tab = '| X[0.6, c] | X[1,c] | X[3,l] |'
    tab = tab + ' X[1,c] |' * (len(Lines[0]) - 3)
    # Opciones de tabla latex
    geometry_options = {'a4paper': True, 'landscape': True,
                        'top': '40pt',
                        'left': '30pt',
                        'right': '30pt',
                        'bottom': '40pt'}

    genenerate_longtabu(Lines, titulo, oname, tab, geometry_options)
    return titulo, oname


def asistencia_clase(fname, sheet_name, titulo, oname, odir):

    date = time.strftime("%d/%m/%Y")

    # Leer datos de planilla
    Lines, comision_, materia = read_data(fname, sheet_name)
    for l in Lines:
        del l[1]                        # remover legajo

    # Remover datos numéricos y obtener comisión
    comisiones = []
    for l in Lines[2:]:
        comisiones.append(int(l[2]))
        del l[2]                        # remover comision
        l[4:] = [' '] * (len(l) - 4)

    for l in Lines[0:2]:
        del l[2]                        # remover comision

    # Organización de la tabla
    k = int((len(Lines[2]) - 2) / 2)
    tab = '| X[1.2, c] | X[7,c] |'
    tab = tab + ' X[0.8,c] |' * k

    # Opciones de tabla latex
    geometry_options = {'a4paper': True, 'landscape': True,
                        'top': '40pt',
                        'left': '30pt',
                        'right': '30pt',
                        'bottom': '40pt'}

    # revisar comiciones
    if set(comisiones).difference(set([1, 2, 3, 4])):
        message = error_comision(Lines, comisiones)
        return message, 'error'

    # Asistencia a Clase de Coloquio
    titulo = 'Asistencia a Clase Coloquio ' \
             '- {0} - {1}'.format(materia, date)
    oname = 'Asist_Col_{0}_{1}'
    if materia == 'Cálculo en una Variable':
        oname = oname.format('Calc', date.replace('/', ''))
    else:
        oname = oname.format('Alg', date.replace('/', ''))
    oname = os.path.join(odir, oname)
    generara_tabla_asistencia(Lines, titulo, oname, geometry_options, tab)

    # Asistencia a Clase de Teoria
    titulo = 'Asistencia a Clase Teoria ' \
             '- {0} - {1}'.format(materia, date)
    oname = 'Asist_Teo_{0}_{1}'
    if materia == 'Cálculo en una Variable':
        oname = oname.format('Calc', date.replace('/', ''))
    else:
        oname = oname.format('Alg', date.replace('/', ''))
    oname = os.path.join(odir, oname)
    generara_tabla_asistencia(Lines, titulo, oname, geometry_options, tab)

    # Imprimir por comision Practica
    n_com = int(len(set(comisiones)))
    for j in range(1, n_com + 1):
        # Titulo de archivo de salida y nombre
        titulo = 'Asistencia a Clase Práctica Comision ' \
                 '{0} - {1} - {2}'.format(j, materia, date)
        oname = 'Asist_Prac_Com{0}_{1}_{2}'
        if materia == 'Cálculo en una Variable':
            oname = oname.format(j, 'Calc', date.replace('/', ''))
        else:
            oname = oname.format(j, 'Alg', date.replace('/', ''))
        oname = os.path.join(odir, oname)

        TCom = [Lines[0], Lines[1]]
        for i in range(0, len(comisiones)):
            if j == comisiones[i]:
                TCom.append(Lines[i + 2])

        generara_tabla_asistencia(TCom, titulo, oname, geometry_options, tab)

    titulo = 'Asistencias a todas las clases'
    oname = odir
    return titulo, oname


def main(fname, odir, reporte):

    if reporte == 'p1':
        titulo = 'Resultados Primer Parcial - {0} - {1}'
        sheet_name = 'Primer Parcial'
        oname = 'Resultados_P1_{0}_{1}'
        [titulo, oname] = parciales_recuperatorios(fname, sheet_name, titulo,
                                                   oname, odir)
    elif reporte == 'p2':
        titulo = 'Resultados Segundo Parcial - {0} - {1}'
        sheet_name = 'Segundo Parcial'
        oname = 'Resultados_P2_{0}_{1}'
        [titulo, oname] = parciales_recuperatorios(fname, sheet_name, titulo,
                                                   oname, odir)
    elif reporte == 'p3':
        titulo = 'Resultados Tercer Parcial - {0} - {1}'
        sheet_name = 'Tercer Parcial'
        oname = 'Resultados_P3_{0}_{1}'
        [titulo, oname] = parciales_recuperatorios(fname, sheet_name, titulo,
                                                   oname, odir)
    elif reporte == 'r1':
        titulo = 'Resultados Recuperatorio Primer Parcial - {0} - {1}'
        sheet_name = 'Rec. Primer Parcial'
        oname = 'Resultados_RP1_{0}_{1}'
        [titulo, oname] = parciales_recuperatorios(fname, sheet_name, titulo,
                                                   oname, odir)
    elif reporte == 'r2':
        titulo = 'Resultados Recuperatorio Segundo Parcial - {0} - {1}'
        sheet_name = 'Rec. Segundo Parcial'
        oname = 'Resultados_RP2_{0}_{1}'
        [titulo, oname] = parciales_recuperatorios(fname, sheet_name, titulo,
                                                   oname, odir)
    elif reporte == 'r3':
        titulo = 'Resultados Recuperatorio Tercer Parcial - {0} - {1}'
        sheet_name = 'Rec. Tercer Parcial'
        oname = 'Resultados_RP3_{0}_{1}'
        [titulo, oname] = parciales_recuperatorios(fname, sheet_name, titulo,
                                                   oname, odir)
    elif reporte == 'cpr':
        titulo = 'Condición Antes de Recuperatorios - {0} - {1}'
        sheet_name = 'PreRec'
        oname = 'Condicion_PreRec_{0}_{1}'
        [titulo, oname] = condicion_prerec(fname, sheet_name, titulo, oname,
                                           odir)
    elif reporte == 'cf':
        titulo = 'Condición Final - {0} - {1}'
        sheet_name = 'General'
        oname = 'Condicion_Final_{0}_{1}'
        [titulo, oname] = condicion_final(fname, sheet_name, titulo, oname,
                                          odir)
    elif reporte == 'as':
        titulo = 'Asistencia Parciales y Recuperatorios - {0} - {1}'
        sheet_name = 'General'
        oname = 'Asistencia_Parciales_{0}_{1}'
        [titulo, oname] = asistencia_parciales(fname, sheet_name, titulo,
                                               oname, odir)

    elif reporte == 'asc':
        titulo = 'Asistencia a Clase {0} - {1} - {2}'
        sheet_name = 'Asistencia'
        oname = 'Asistencia_Clase_{0}_{1}_{2}'
        [titulo, oname] = asistencia_clase(fname, sheet_name, titulo, oname,
                                           odir)
    return titulo, oname


if __name__ == '__main__':
    main('Planilla_Alg_C2_29072018.xlsx', './', 'asc')
