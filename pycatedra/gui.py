#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
pycatedra.py
    Interfaz gráfica para generar la planilla de cursado de cátedra.

    funciones:
        1. Generar Planilla
           Script para generar la planilla de resultados a partir de los
           archivos descargados del SIU GUARANI.
        2. Script para publicar resultados contenidos en la planilla de
           resltados.
'Juan Felipe Restrepo <jrestrepo@ingenieria.une.edu.ar>'
2018-07-16
'''

import os
from tkinter import Tk, filedialog, Frame, Text, Button, StringVar, Radiobutton
from tkinter import Label, WORD, W, END, LEFT
from .planilla import main as planilla
from .publicar import main as publicar


class Application(Frame):

    def __init__(self, master):
        Frame.__init__(self, master)
        self.grid()
        self.create_widgets()
        self.inputdir = ''
        self.outputdir = ''
        self.infile = ''

    def create_widgets(self):

        # Radio Buttons - Opciones para Publicar
        self.report = StringVar(None, 'as')
        texto = ['Asistencia Clase', 'Asistencia Parciales', 'Primer Parcial',
                 'Segundo Parcial', 'Tercer Parcial', 'Condición antes Rec.',
                 'Rec. Primer Parcial', 'Rec. Segundo Parcial',
                 'Rec. Tercer Parcial', 'General']

        valores = ['asc', 'as', 'p1', 'p2', 'p3', 'cpr', 'r1', 'r2', 'r3',
                   'cf']
        filas = [0, 1, 0, 1, 2, 0, 0, 1, 2, 0]
        columnas = [0, 0, 1, 1, 1, 2, 3, 3, 3, 4]

        for i, j, k, w in zip(texto, valores, filas, columnas):
            Radiobutton(self,
                        text=i,
                        variable=self.report,
                        value=j).grid(row=k, column=w, sticky=W)

        # Caja de Texto - Salidas
        self.tbox = Text(self, width=90, height=5, wrap=WORD)
        self.tbox.grid(row=5, column=0, columnspan=6)

        # Boton 1 - Generar Planilla
        self.button1 = Button(self, text='Generar Planilla', width=15)
        self.button1.grid(row=7, column=0)
        self.button1['command'] = self.generar_planilla

        # Boton 2 - Publicar Resultados
        self.button2 = Button(self, text='Publicar Resultados', width=15)
        self.button2['command'] = self.publicar_reporte
        self.button2.grid(row=8, column=0)

        # Label - Instrucciones
        self.label0 = Label(self, text='Instrucciones:', anchor=W,
                            justify=LEFT, font='Helvetica 14 bold')
        self.label0.grid(row=6, column=1, columnspan=2, sticky=W)

        Inst1 = 'Generar Planilla:\n\t' + \
                '1. Seleccionar la carpeta que contiene ' + \
                'los 3 archivos .xls (Bioing., Bioinfo. y Transp.)\n\t    ' + \
                'descargados del SIU GUARANI.\n\t' + \
                '2. Seleccionar el directorio para guardar la planilla.'
        self.label1 = Label(self, text=Inst1, anchor=W, justify=LEFT, width=90)
        self.label1.grid(row=7, column=1, columnspan=6)

        Inst2 = 'Publicar Resultados:\n\t' + \
                '1. Seleccionar el tipo de resultados a publicar ' + \
                '(Asistencia, Primer Parcial, etc).\n\t' + \
                '2. Seleccionar el archivo .xlsx que contiene los ' + \
                'resultados.\n\t' + \
                '3. Seleccionar el directorio para guardar la publicación.'
        self.label2 = Label(self, text=Inst2, anchor=W, justify=LEFT, width=90)
        self.label2.grid(row=8, column=1, columnspan=6)

    def generar_planilla(self):
        self.inputdir = filedialog.askdirectory(title='Directorio Archivos '
                                                'del SIU GUARANI')
        os.chdir(self.inputdir)
        self.outputdir = filedialog.askdirectory(title='Seleccionar Directorio'
                                                 ' de Salida')
        tit, oname = planilla(self.inputdir, self.outputdir)
        message = 'Planilla Generada: {0}\narchivo: {1}'.format(tit, oname)
        self.tbox.delete(0.0, END)
        self.tbox.insert(0.0, message)

    def publicar_reporte(self):
        self.infile = filedialog.askopenfilename(initialdir=os.getcwd(),
                                                 title='Seleccionar Archivo '
                                                       'de Resultados',
                                                 filetypes=(('archivos exel',
                                                             '*.xlsx'),
                                                            ('all files',
                                                             '*.*')))
        self.output = filedialog.askdirectory(title='Seleccionar Directorio de'
                                              ' Salida')
        tit, oname = publicar(self.infile, self.output, self.report.get())

        if oname == 'error':
            message = tit
            self.tbox.delete(0.0, END)
            self.tbox.insert(0.0, message)
            return None

        if self.report.get() == 'asc':
            message = '{0}\npublicado en la carpeta: {1}/'.format(tit, oname)
        else:
            message = '{0}\npublicado en: {1}.pdf'.format(tit, oname)
        self.tbox.delete(0.0, END)
        self.tbox.insert(0.0, message)


def main():
    root = Tk()
    root.title('pyCatedra - Generador de planillas')
    root.geometry('800x350')
    Application(root)
    root.mainloop()


if __name__ == '__main__':
    main()
