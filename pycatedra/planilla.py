#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
planilla.py
    Script para generar la planilla de resultados a partir de los archivos
    descargados del SIU GUARANI

'Juan Felipe Restrepo <jrestrepo@ingenieria.une.edu.ar>'
2018-07-16
'''
import time
import xlsxwriter
import xlrd
import unidecode
import itertools
import string
import os
import glob
import os


def find_between(s, first, last):
    try:
        start = s.index(first) + len(first)
        end = s.index(last, start)
        return s[start:end]
    except ValueError:
        return None


def _column_name_generator():
    for i in range(1, 3):
        for p in itertools.product(string.ascii_uppercase, repeat=i):
            yield ''.join(p)


def set_col_color(range_, color_, temp_sheet_):
    format_col = owb.add_format()
    format_col.set_pattern(1)
    format_col.set_border(1)
    format_col.set_bg_color(color_)
    temp_sheet_.conditional_format(range_, {'type': 'cell', 'criteria': '>=',
                                            'value': 0, 'format': format_col})
    temp_sheet_.conditional_format(range_, {'type': 'cell', 'criteria': '<',
                                            'value': 0, 'format': format_col})


def read_data(fname):
    global legajo, alumno, carrera, matid, materia, N

    wb = xlrd.open_workbook(fname)
    wb_sheet = wb.sheet_by_name('Reporte')
    materia = find_between(wb_sheet.cell(5, 0).value, ':', '(')

    temp = wb_sheet.col_slice(colx=0, start_rowx=13)
    for i in temp:
        legajo.append(i.value)

    temp = wb_sheet.col_slice(colx=1, start_rowx=13)
    for i in temp:
        alumno.append(i.value)

    temp = wb_sheet.col_slice(colx=4, start_rowx=13)
    for i in temp:
        if i.value == u'Bioingeniería':
            carrera.append('B')
        elif i.value == u'Bioinformática':
            carrera.append('L')
        elif i.value == 'Transporte':
            carrera.append('T')
    N = len(alumno)


def Asistencia():
    temp_sheet = owb.add_worksheet('Asistencia')
    temp_sheet.merge_range('A1:J1', 'Planilla de Asistencia', merge_format)
    temp_sheet.merge_range('A2:J2', materia, merge_format)
    temp_sheet.write(2, 0, 'Carrera', c_format)
    temp_sheet.write(2, 1, 'Legajo', c_format)
    temp_sheet.write(2, 2, 'Alumno', c_format)
    temp_sheet.write(2, 3, 'Comisión', c_format)

    r = list(_column_name_generator())
    count = 4
    for i in range(1, 15):
        ran = '{0}3:{1}3'.format(r[count], r[count + 1])
        temp_sheet.merge_range(ran, 'S-{0}'.format(i), merge_format)
        temp_sheet.write(3, 2 * i + 2, '1', merge_format)
        temp_sheet.write(3, 2 * i + 3, '2', merge_format)
        count += 2

    set_col_color('A4:D4', '#F4DDD7', temp_sheet)

    row = 4
    for i, j, k in zip(carrera, legajo, alumno):
        temp_sheet.write(row, 0, i, format_1)
        temp_sheet.write(row, 1, j, format_2)
        temp_sheet.write(row, 2, k, format_2)
        temp_sheet.write(row, 3, 0, format_1)
        temp_sheet.set_row(row, None, format_1)
        row += 1


def Parciales(Heads):
    count = 0
    for k in Heads:
        temp_sheet = owb.add_worksheet(Heads[count])
        temp_sheet.merge_range('A1:J1', Heads[count], merge_format)
        temp_sheet.merge_range('A2:J2', materia, merge_format)
        temp_sheet.write(2, 0, 'Carrera', c_format)
        temp_sheet.write(2, 1, 'Legajo', c_format)
        temp_sheet.write(2, 2, 'Alumno', c_format)

        temp_sheet.write(2, 3, 'Comisión', c_format)

        temp_sheet.write(2, 4, 'Asistencia', c_format)
        set_col_color('E3:E{0}'.format(N + 3), '#F4DDD7', temp_sheet)
        temp_sheet.set_column('D:D', 20, c_format)

        temp_sheet.write(2, 5, 'E1', c_format)
        set_col_color('F3:F{0}'.format(N + 3), '#66B2FF', temp_sheet)

        temp_sheet.write(2, 6, 'E2', c_format)
        set_col_color('G3:G{0}'.format(N + 3), '#FF9999', temp_sheet)

        temp_sheet.write(2, 7, 'E3', c_format)
        set_col_color('H3:H{0}'.format(N + 3), '#CCFFCC', temp_sheet)

        temp_sheet.write(2, 8, 'E4', c_format)
        set_col_color('I3:I{0}'.format(N + 3), '#E0E0E0', temp_sheet)

        temp_sheet.write(2, 9, 'E5', c_format)
        set_col_color('J3:J{0}'.format(N + 3), '#CCCCFF', temp_sheet)

        temp_sheet.write(2, 10, 'E6', c_format)
        set_col_color('K3:K{0}'.format(N + 3), '#66B2FF', temp_sheet)

        temp_sheet.write(2, 11, 'Total', c_format)
        set_col_color('L3:L{0}'.format(N + 3), '#FFE5CC', temp_sheet)

        row = 3
        format_ = owb.add_format()
        format_.set_border(1)
        for i, j, k in zip(carrera, legajo, alumno):
            temp_sheet.write_formula('D{0}'.format(row + 1),
                                     "='Asistencia'!D{0}".format(row + 2),
                                     format_1)
            temp_sheet.write(row, 0, i, format_1)
            temp_sheet.write(row, 1, j, format_2)
            temp_sheet.write(row, 2, k, format_2)
            temp_sheet.write(row, 11, '=SUM(F{0}:K{0})'.format(row + 1),
                             format_)
            row += 1
        count += 1


def PreRecuperatorio():
    temp_sheet = owb.add_worksheet('PreRec')

    range0_ = 'A1:J1'
    range1_ = 'A2:J2'
    if recursada:
        range0_ = 'A1:H1'
        range1_ = 'A2:H2'

    temp_sheet.merge_range(range0_, 'Planilla Antes de Recuperatorios',
                           merge_format)
    temp_sheet.merge_range(range1_, materia, merge_format)

    temp_sheet.write(2, 0, 'Carrera', c_format)
    temp_sheet.write(2, 1, 'Legajo', c_format)
    temp_sheet.write(2, 2, 'Alumno', c_format)
    temp_sheet.write(2, 3, 'Comisión', c_format)

    temp_sheet.write(2, 4, 'P1', c_format)
    set_col_color('E3:E{0}'.format(N + 3), '#66B2FF', temp_sheet)
    temp_sheet.write(2, 5, 'P2', c_format)
    set_col_color('F3:F{0}'.format(N + 3), '#FF9999', temp_sheet)

    temp_sheet.write(2, 6, 'Nota', c_format)
    temp_sheet.write(2, 7, 'Condición', c_format)
    temp_sheet.write(2, 8, 'Opciones', c_format)

    if not recursada:
        temp_sheet.write(2, 6, 'P3', c_format)
        set_col_color('G3:G{0}'.format(N + 3), '#CCFFCC', temp_sheet)
        temp_sheet.write(2, 7, 'Nota', c_format)
        set_col_color('H3:H{0}'.format(N + 3), '#FFE5CC', temp_sheet)
        temp_sheet.write(2, 8, 'Condición', c_format)
        temp_sheet.write(2, 9, 'Opciones', c_format)
    else:
        set_col_color('G3:G{0}'.format(N + 3), '#FFE5CC', temp_sheet)

    row = 3
    for i, j, k in zip(carrera, legajo, alumno):
        temp_sheet.write_formula('D{0}'.format(row + 1),
                                 "='Asistencia'!D{0}".format(row + 2),
                                 format_1)
        temp_sheet.write_formula('E{0}'.format(row + 1),
                                 "='Primer Parcial'!L{0}".format(row + 1))
        temp_sheet.write_formula('F{0}'.format(row + 1),
                                 "='Segundo Parcial'!L{0}".format(row + 1))

        temp_sheet.write(row, 6, '=SUM(E{0}:F{0})'.format(row + 1))
        if not recursada:
            temp_sheet.write_formula('G{0}'.format(row + 1),
                                     "='Tercer Parcial'!L{0}".format(row + 1))
            temp_sheet.write(row, 7, '=SUM(E{0}:G{0})'.format(row + 1))
        temp_sheet.write(row, 0, i, format_1)
        temp_sheet.write(row, 1, j, format_2)
        temp_sheet.write(row, 2, k, format_2)
        row += 1


def General():
    temp_sheet = owb.add_worksheet('General')

    range0_ = 'A1:J1'
    range1_ = 'A2:J2'
    if recursada:
        range0_ = 'A1:I1'
        range1_ = 'A2:I2'
    temp_sheet.merge_range(range0_, 'Planilla General', merge_format)
    temp_sheet.merge_range(range1_, materia, merge_format)
    temp_sheet.write(2, 0, 'Carrera', c_format)
    temp_sheet.write(2, 1, 'Legajo', c_format)
    temp_sheet.write(2, 2, 'Alumno', c_format)

    temp_sheet.write(2, 3, 'Comisión', c_format)

    temp_sheet.write(2, 4, 'P1', c_format)
    set_col_color('E2:E{0}'.format(N + 3), '#66B2FF', temp_sheet)

    temp_sheet.write(2, 5, 'P2', c_format)
    set_col_color('F2:F{0}'.format(N + 3), '#FF9999', temp_sheet)

    if not recursada:
        temp_sheet.write(2, 6, 'P3', c_format)
        set_col_color('G2:G{0}'.format(N + 3), '#CCFFCC', temp_sheet)

        temp_sheet.write(2, 7, 'RP1', c_format)
        set_col_color('H2:H{0}'.format(N + 3), '#E0E0E0', temp_sheet)

        temp_sheet.write(2, 8, 'RP2', c_format)
        set_col_color('I2:I{0}'.format(N + 3), '#CCCCFF', temp_sheet)

        temp_sheet.write(2, 9, 'RP3', c_format)
        set_col_color('J2:J{0}'.format(N + 3), '#66B2FF', temp_sheet)

        temp_sheet.write(2, 10, 'Nota Final', c_format)
        set_col_color('K2:K{0}'.format(N + 3), '#FFE5CC', temp_sheet)

        temp_sheet.write(2, 11, 'Condición Final', c_format)
    else:
        temp_sheet.write(2, 6, 'RP1', c_format)
        set_col_color('F2:F{0}'.format(N + 3), '#E0E0E0', temp_sheet)

        temp_sheet.write(2, 7, 'RP2', c_format)
        set_col_color('G2:G{0}'.format(N + 3), '#CCCCFF', temp_sheet)

        temp_sheet.write(2, 8, 'Nota Final', c_format)
        set_col_color('H2:H{0}'.format(N + 3), '#FFE5CC', temp_sheet)

        temp_sheet.write(2, 9, 'Condición Final', c_format)

    row = 3
    for i, j, k in zip(carrera, legajo, alumno):
        temp_sheet.write_formula('D{0}'.format(row + 1),
                                 "='Asistencia'!D{0}".format(row + 2),
                                 format_1)
        temp_sheet.write_formula('E{0}'.format(row + 1),
                                 "='Primer Parcial'!L{0}".format(row + 1))
        temp_sheet.write_formula('F{0}'.format(row + 1),
                                 "='Segundo Parcial'!L{0}".format(row + 1))
        if not recursada:
            temp_sheet.write_formula('G{0}'.format(row + 1),
                                     "='Tercer Parcial'!L{0}".format(row + 1))
            temp_sheet.write_formula('H{0}'.format(row + 1),
                                     "='Rec. Primer Parcial'!L{0}".
                                     format(row + 1))
            temp_sheet.write_formula('I{0}'.format(row + 1),
                                     "='Rec. Segundo Parcial'!L{0}"
                                     .format(row + 1))
            temp_sheet.write_formula('J{0}'.format(row + 1),
                                     "='Rec. Tercer Parcial'!L{0}".
                                     format(row + 1))
            temp_sheet.write(row, 10, '=SUM(MAX(E{0},H{0}),MAX(F{0},I{0}),'
                                      'MAX(F{0},I{0}))/3'.format(row + 1))
        else:
            temp_sheet.write_formula('G{0}'.format(row + 1),
                                     "='Rec. Primer Parcial'!L{0}".
                                     format(row + 1))
            temp_sheet.write_formula('H{0}'.format(row + 1),
                                     "='Rec. Segundo Parcial'!L{0}"
                                     .format(row + 1))
            temp_sheet.write(row, 8, '=SUM(MAX(E{0},G{0}),'
                                     'MAX(F{0},H{0}))/2'.format(row + 1))

        temp_sheet.write(row, 0, i, format_1)
        temp_sheet.write(row, 1, j, format_2)
        temp_sheet.write(row, 2, k, format_2)
        row += 1


def main(indir, outputdir):
    global alumno, legajo, carrera, recursada, materia, merge_format
    global c_format, format_1, format_2, format_3, owb

    legajo = []
    alumno = []
    carrera = []
    recursada = False
    materia = ''

    fname = glob.glob(indir + "/*.xls")
    print(fname)
    for i in fname:
        read_data(i)
    date = time.strftime("%d/%m/%Y")
    mont = time.strftime("%m")
    mat = unidecode.unidecode(materia).strip()

    if int(mont) < 6:
        cuat = 'C1'
    else:
        cuat = 'C2'
    if (mat[0] == 'C' and cuat == 'C2') or (mat[0] == 'A' and cuat == 'C1'):
        recursada = True

    oname = 'Planilla_{0}_{1}_{2}.xlsx'.format(mat[:3], cuat,
                                               date.replace('/', ''))
    oname = os.path.join(outputdir, oname)

    owb = xlsxwriter.Workbook(oname)
    c_format = owb.add_format({'bold': 1, 'align': 'center', 'border': 1})
    merge_format = owb.add_format({'bold': 1, 'border': 1, 'align': 'center',
                                   'valign': 'vcenter'})
    format_1 = owb.add_format({'bold': 1, 'border': 1, 'align': 'center',
                               'valign': 'vcenter'})
    format_2 = owb.add_format({'bold': 1, 'border': 1, 'align': 'left',
                               'valign': 'vcenter'})
    format_3 = owb.add_format({'bold': 0, 'border': 1, 'align': 'left',
                               'valign': 'vcenter'})

    Asistencia()
    if recursada:
        Parciales(('Primer Parcial', 'Segundo Parcial'))
    else:
        Parciales(('Primer Parcial', 'Segundo Parcial', 'Tercer Parcial'))
    PreRecuperatorio()
    if recursada:
        Parciales(('Rec. Primer Parcial', 'Rec. Segundo Parcial'))
    else:
        Parciales(('Rec. Primer Parcial', 'Rec. Segundo Parcial',
                   'Rec. Tercer Parcial'))
    General()

    titulo = '{0} - Cuatrimestre {1} - {2}'.format(mat, cuat, date)
    print('Planilla Generada: ' + titulo)
    owb.close()
    return titulo, oname


if __name__ == '__main__':
    inputdir = os.path.dirname(__file__)
    os.chdir(inputdir)
    outputdir = './'
    main(inputdir, './')
