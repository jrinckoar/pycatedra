#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
from .gui import main as gui


def main(args=None):
    if args is None:
        args = sys.argv[1:]

    gui()

if __name__ == "__main__":
    main()
